package com.oleh.beans.interfaces;

public interface BeanValidator {
    boolean validate();
}
