package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;

public class BeanF implements BeanValidator {

    private String name;
    private int value;

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public boolean validate() {
        System.out.println("validate F");
        if (name!=null && value>0){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
