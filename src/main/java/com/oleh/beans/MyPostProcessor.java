package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyPostProcessor implements BeanPostProcessor {
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof BeanValidator){
            boolean validate = ((BeanValidator) bean).validate();
            if (validate){
                System.out.println("Bean "+ beanName+"  is valid");
                return bean;
            }
            else {
                System.out.println("Bean "+ beanName+" is invalid");
                return null;
            }
        }
        return bean;
    }
}
