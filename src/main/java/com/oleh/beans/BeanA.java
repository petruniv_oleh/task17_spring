package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    private String name;
    private int value;

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public boolean validate() {
        System.out.println("validate A");
        if (name!=null && value>0){
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void destroy() throws Exception {
        System.out.println("invoke destroy method in beanA");
    }

    public void afterPropertiesSet() throws Exception {
        System.out.println("invoke afterPropertiesSet method in beanA");
    }
}
