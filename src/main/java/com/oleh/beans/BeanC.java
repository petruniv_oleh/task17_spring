package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;

public class BeanC implements BeanValidator {
    private String name;
    private int value;

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }


    public void init(){
        System.out.println("beanC initializing");
    }

    public void destroy(){
        System.out.println("beanB destroy");
    }

    public boolean validate() {
        System.out.println("validate C");
        if (name!=null && value>0){
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
