package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;

public class BeanB implements BeanValidator {

    private String name;
    private int value;

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void init(){
        System.out.println("beanB initializing");
    }

    public void initNew(){
        System.out.println("beanB new initializing");
    }

    public void destroy(){
        System.out.println("beanB destroy");
    }

    public boolean validate() {
        System.out.println("validate B");
        if (name!=null && value>0){
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
