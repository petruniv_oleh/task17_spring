package com.oleh.beans;

import com.oleh.beans.interfaces.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {

    private String name;
    private int value;

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public boolean validate() {
        System.out.println("validate E");
        if (name!=null && value>0){
            return true;
        }
        return false;
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("BeanE postConstruct");
    }

    @PreDestroy
    public void preDestroy(){
        System.out.println("BeanE preDestroy");
    }


    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
