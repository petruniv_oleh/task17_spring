package com.oleh.config;

import com.oleh.beans.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("beans.properties")
public class BeanConfig1 {

    @Value("${beanB.name}")
    private String beanBName;
    @Value("${beanC.name}")
    private String beanCName;
    @Value("${beanD.name}")
    private String beanDName;

    @Value("${beanB.value}")
    private int beanBValue;
    @Value("${beanC.value}")
    private int beanCValue;
    @Value("${beanD.value}")
    private int beanDValue;

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanD")
    public BeanB getBeanB(){
        return new BeanB(beanBName, beanBValue);
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanB")
    public BeanC getBeanC(){
        return new BeanC(beanCName, beanCValue);
    }


    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD(){
        return new BeanD(beanDName, beanDValue);
    }



}
