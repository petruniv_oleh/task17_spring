package com.oleh.config;

import com.oleh.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(BeanConfig1.class)
public class BeanConfig2 {

    @Qualifier("beanA1")
    @Bean
    public BeanA getBeanAFromBC(BeanB beanB, BeanC beanC){
        return new BeanA(beanB.getName(), beanC.getValue());
    }
    @Qualifier("beanA2")
    @Bean
    public BeanA getBeanAFromBD(BeanB beanB, BeanD beanD){
        return new BeanA(beanB.getName(), beanD.getValue());
    }
    @Qualifier("beanA3")
    @Bean
    public BeanA getBeanAFromCD(BeanB beanC, BeanD beanD){
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean
    public BeanE getBeanEFromABC(@Qualifier("beanA1") BeanA beanA){
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Bean
    public BeanE getBeanEFromABD(@Qualifier("beanA2") BeanA beanA){
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Bean
    public BeanE getBeanEFromACD(@Qualifier("beanA3") BeanA beanA){
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Lazy
    @Bean
    public BeanF getBeanF(){
        return new BeanF("Lutic", 0);
    }

    @Bean(name = "FactoryPostProcessor", initMethod = "init")
    public MyFactoryPostProcessor getMyFactoryPostProcessor(){
        return new MyFactoryPostProcessor();
    }

    @Bean(name = "PostProcessor")
    public MyPostProcessor getMyPostProcessor(){
        return new MyPostProcessor();
    }

}
