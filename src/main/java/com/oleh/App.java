package com.oleh;

import com.oleh.config.BeanConfig2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig2.class);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanName: beanDefinitionNames
             ) {
            System.out.println(beanName);
            Object bean = context.getBean(beanName);
            System.out.println(bean);

        }
    }
}
